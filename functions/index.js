const functions = require("firebase-functions");
const admin = require('firebase-admin');
admin.initializeApp();
const express = require('express');
const cors = require('cors');
const axios = require('axios');
const qs = require('qs');
require('dotenv').config();

const sanitizer = require('express-sanitizer');
const app = express();
app.use(cors({ origin: true }));
app.use(sanitizer());
app.post('/say-hello', async (req, res) => {
    // verify recaptcha
    rr = await axios({
        method: 'POST',
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        data: qs.stringify({
            secret: process.env.GRECAPTCHA_SECRET,
            response: req.body.grecaptcha_response
        }),
        url: 'https://www.google.com/recaptcha/api/siteverify'
    });
    // if successful and score is trustful write in databse
    if (rr.data.success && rr.data.score >= 0.5) {
        const s = req.sanitize;
        const doc = { from: s(req.body.email), subject: s(req.body.subject), message: s(req.body.message_body) };
        // admin bypass firestore rules
        await admin.firestore().collection('messages').add(doc);
        res.sendStatus(201);
    } else {
        error = rr.data['error-codes'].map(e => {
            switch (e) {
                case 'missing-input-secret':
                    return 'The secret parameter is missing.';
                case 'invalid-input-secret':
                    return 'The secret parameter is invalid or malformed.';
                case 'missing-input-response':
                    return 'The response parameter is missing.';
                case 'invalid-input-response':
                    return 'The response parameter is invalid or malformed.';
                case 'bad-request':
                    return 'The request is invalid or malformed.';
                case 'timeout-or-duplicate':
                    return 'The response is no longer valid: either is too old or has been used previously.';
                default:
                    return 'Unknown';
            }
        });
        console.log(error);
        res.status(500).json({ error: 'Error' });
    }
});

exports.api = functions.https.onRequest(app);