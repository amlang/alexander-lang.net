# alexander-lang.net
Hi there 👋,   
this website is a personal project of mine.   
I've evolved quite a bit as a developer over the past few years and I wanted to create something that shows what I've learned and done in this time.   
Let's see how the project envolves in the future.

##  Getting started
### Develop
```bash
$> npm install
$> npm run sass:dev
$> firebase emulators:start
$> open http://localhost:5000 
```

### Build
```bash
$> npm run sass:build
```

### Deploy
#### Preview
```bash
$> firebase hosting:channel:deploy pre-release-preview
```

#### Prod
```bash
$> firebase hosting:deploy
```

## What I've used
### Tools
* [Visual Studio Code](https://code.visualstudio.com/) my "Swiss Army Editor" 🤩
* [Sass](https://sass-lang.com/) for a modular CSS
* [Fira Mono](https://fonts.google.com/specimen/Fira+Mono?query=fira+mono) and [Roboto Mono](https://fonts.google.com/specimen/Roboto+Mono?query=roboto+mono) as font for all texts
* [Font Awesome](https://fontawesome.com/) and [Material Design Icons](https://dev.materialdesignicons.com/icons) for "About me" Icons
* [reCAPTCHA](https://www.google.com/recaptcha/admin/site/559304544) to protect my site from fraudulent activities and spam
* [Firebase Hosting](https://firebase.google.com/) easy deploy and hosting and [Firebase Functions](https://firebase.google.com/) as zero effort backend until I created a new one
* [avif.io](https://avif.io/) to create an avif version of the images
* [brew mkcert](https://formulae.brew.sh/formula/mkcert) to create test SSL cert
* [DSGVO Gesetz](https://dsgvo-gesetz.de/) 
* [brew webp](https://formulae.brew.sh/formula/webp) to create webp Image
```bash
$> brew install webp
$> cwebp -q 100 portrait@2x.png -o portrait@2x.webp
```
* [https://www.einfach-abmahnsicher.de/](Einfach Abmahnsicher)
* [https://boxy-svg.com](Boxy SVG) to create the 404 Ghost; not beautiful but rares