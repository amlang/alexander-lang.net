(() => {
    const SITE_KEY = '6LdgT1YhAAAAAKCy2C3egkz2Zwn_LQS9IJU9xSAg';
    // cookie-banner
    const cc = JSON.parse(localStorage.getItem('consent_cookies'));
    const cookie_banner = document.querySelector('#cookie-banner');

    const activate_recaptcha = () => {
        document.querySelector('#say-hello-form').classList.remove('hidden');
        document.querySelector('#say-hello-form').previousElementSibling.classList.add('hidden');

        const script = document.createElement('script');
        script.async = true;
        script.defer = true;
        script.src = `https://www.google.com/recaptcha/api.js?render=${SITE_KEY}`;
        document.body.append(script);
    };

    const open_cookie_banner = async () => {
        if (!cc || new Date(cc.valid_until) <= new Date()){
            localStorage.removeItem('consent_cookies');
            resp = await fetch('/cookie-banner.html');
            if(resp.ok){
                document.querySelector('.grid').classList.add('cookie-banner-opened');
                cookie_banner.innerHTML = await resp.text();
                eval(cookie_banner.querySelector('script').innerText);
                cookie_banner.classList.remove('hidden');
            }
        } else {
            if (cc.grecaptcha){
                activate_recaptcha();
            }
        }
    };
    
    if (!document.querySelector('#loading')){
        open_cookie_banner();
    }

    header = document.querySelector('header');
    document.addEventListener('scroll', e => {
        title = header.querySelector('p.title');
        if (window.pageYOffset > 50 && !header.classList.contains('scrolled')) {
            header.classList.add('scrolled');
            title.classList.remove('dont-show');
            title.ariaHidden = false;
        } else if (window.pageYOffset <= 50 && header.classList.contains('scrolled')) {
            header.classList.remove('scrolled');
            title.classList.add('dont-show');
            title.ariaHidden = true;
        }
    });

    buttons = document.querySelectorAll('.companies button[role="tab"]');
    buttons.forEach(button => {
        button.addEventListener('click', (e) => {
            document.querySelectorAll('.jobs .job-panel').forEach(e => e.ariaHidden = true);
            document.getElementById(e.srcElement.getAttribute('aria-controls')).ariaHidden = false;
            buttons.forEach(e => {
                e.ariaSelected = false;
                e.ariaExpanded = false;
            });
            e.srcElement.ariaSelected = true
            e.srcElement.ariaExpanded = true
        });
    });

    textarea = document.querySelector('#say-hello-body');
    textarea.addEventListener('input', e => {
        el = e.srcElement;
        el.parentNode.dataset.replicatedValue = el.value;
        document.querySelector(`[data-counter-for="${el.id}"]`).innerText = `${el.value.length}/${el.maxLength}`;
    });

    let timeout;
    good_snackbar = document.querySelector('.snackbar:not(.error)');
    bad_snackbar = document.querySelector('.snackbar.error');
    snackbar_actions = document.querySelectorAll('.snackbar button');
    snackbar_actions.forEach(el => el.addEventListener('click', e => {
        e.preventDefault();
        clearTimeout(timeout);
        e.target.parentElement.ariaHidden = true;
    }));
    const isDev = () => {
        console.log(location.href, location.href.startsWith('http://localhost'));
        return location.href.startsWith('http://localhost');
    }
    form = document.querySelector('#say-hello-form');
    form.addEventListener('submit', async e => {
        e.preventDefault();
        grecaptcha.ready(async () => {
            let token = null;
            try {
                token = await grecaptcha.execute(SITE_KEY, { action: 'submit' })
            } catch (e) {
                console.error(e);
            }
            let o = {};
            new FormData(form).forEach((v, k) => o[k] = v);
            o.grecaptcha_response = token;
            try {
                const BASE_URL = isDev() ? 'http://localhost:5001/alexander-lang-website/us-central1': 'https://us-central1-alexander-lang-website.cloudfunctions.net';
                const resp = await fetch(`${BASE_URL}/api/say-hello`, {
                    method: form.method,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(o)
                });
                if (resp.status == 201) {
                    snackbar = good_snackbar;
                } else {
                    snackbar = bad_snackbar;
                }
            } catch (e) {
                console.error(e);
                snackbar = bad_snackbar;
            } finally {
                form.reset();
                snackbar.ariaHidden = false;
                timeout = setTimeout(() => snackbar.ariaHidden = true, 5000);
            }
        });
    });
    const show_content = () => {
        document.querySelector('#loading')?.remove();
        const grid = document.querySelector('.grid');
        grid.classList.remove('hidden');
        setTimeout(()=> {
            grid.ariaHidden = false;
            open_cookie_banner();
        } , 250);
    };
    const sp = new URLSearchParams(window.location.search);
    const ld = sp.get('nl') != 't';
    if (!ld) {
        show_content();
    }
    window.addEventListener('load', () => {
        setTimeout(() => show_content(), 1500);
    });
})();

