(()=>{
    // darkmode toggle
    root_dataset = document.querySelector('html').dataset;
    darkmode_toggle_btn = document.querySelector('#toggle_darkmode');
    darkmode_toggle_icon = darkmode_toggle_btn.querySelector('span');
    const darkmode_toggle = (isLightmode, save_preference = false) => {
        if (isLightmode) {
            darkmode_toggle_icon.innerText = 'dark_mode';
            root_dataset.theme = 'light';
        } else {
            darkmode_toggle_icon.innerText = 'light_mode';
            root_dataset.theme = 'dark';
        }
        if (save_preference) {
            localStorage.setItem('selected_theme', root_dataset.theme);
        }
    }
    var last_selected_theme = localStorage.getItem('selected_theme');
    if (last_selected_theme == null) {
        darkmode_toggle(window.matchMedia && window.matchMedia('(prefers-color-scheme: light)').matches);
    } else {
        darkmode_toggle(last_selected_theme == 'light');
    }

    window.matchMedia('(prefers-color-scheme: light)').addEventListener('change', e => darkmode_toggle(e.matches));

    darkmode_toggle_btn.addEventListener('click', e => {
        darkmode_toggle(e.srcElement.innerText == 'light_mode', true);
    });
})()