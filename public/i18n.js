(() => {
    const defaultLocale = 'de';
    const supportedLocales = ["de", "en"];

    let currentLocale;
    let translations = {};


    const isSupportedLocale = locale => supportedLocales.indexOf(locale) > -1;

    /**
     * Retrieve user-preferred locales from the browser
     *
     * @returns array 
     */
    const browserLocales = () => navigator.languages.map(locale => locale.split("-")[0]);

    const fetchLocale = async (lang) => {
        const response = await fetch(`i18n/${lang}.json`);
        return await response.json();
    };

    const setLocale = async (newLocale, save_preference = false) => {
        if (newLocale == currentLocale) return;

        translations = await fetchLocale(newLocale);
        currentLocale = newLocale;
        translatePage();
        if (save_preference) {
            localStorage.setItem('selected_locale', newLocale);
        }
    };

    const translatePage = () => document.querySelectorAll('[data-i18n]').forEach(translate);

    const translate = (el) => {
        const key = el.dataset.i18n;
        const translation = translations[key];
        if (translation !== undefined) {
            el.innerHTML = translation;
        }
    };

    document.addEventListener('DOMContentLoaded', () => {
        var locale = localStorage.getItem('selected_locale') || (browserLocales() || []).find(isSupportedLocale) || defaultLocale

        setLocale(locale);
        switcher = document.querySelector('#header-language-select');
        switcher.value = locale;
        switcher.onchange = (e) => setLocale(e.target.value, true);
    });

})()