# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [1.2.1] - 2022-10-16
### Fixed
* URLs in Sitemap XML changed from `http://` to `https://`
## [1.2.0] - 2022-10-08
### Added
* Sitemap XML
* robots.txt, with `/wp-admin` and `/*.php$` disallowed
### Changed
* `rel="canonical"` tag on `index.html` and `datenschutz.html` added
* `noindex`-meta tag on `datenschutz.html` added
## [1.1.0] - 2022-10-02
### Changed
* 404 Page customized
* Ghost SVG added

## [1.0.1] - 2022-10-01
### Fixed
*  Fix: CI failures
  - Update .gitlab-ci.yml file
  - latest `firebase-functions` added
  - npm dependencies updated
  - npm audit fixed
* Fix: Production base URL used in contanct form 

## [1.0.0] - 2022-10-01
### Added
* Initial static html layout with "Hero", "About me", "Worked at" and "Say hello" section
* Modular Scss for each section, colors, inputs, buttons and typography
  - inspired by the current material design
* Some Javascript methods for
  - switching and saving prefered color themes
  - header scroll animation
  - switching the job panel
* Javascript I18N support, with combobox switching
* Auto-Growing Textare
* Cookie-Banner
* Privacy site